import { Component } from '@angular/core';
import {AuthService} from "./shared/services/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'events-ui';
  navbarState = 'collapse';

  constructor(private auth: AuthService) {}

  toggleNavbar() {
    this.navbarState = this.navbarState === 'collapse' ? 'collapsed' : 'collapse';
  }

  signedInUser() {
    return this.auth.signedInUser();
  }

  signOut() {
    this.auth.signOutUser();
  }
}
