import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  private _apiUrl = 'http://localhost:3000/api';
  private _eventsEndpoint = '/events';
  private _specialEventsEndpoint = '/special';

  constructor(private http: HttpClient) { }

  getEvents(): Observable<any> {
    return this.http.get<any>(`${this._apiUrl}${this._eventsEndpoint}`);
  }

  getSpecialEvents(): Observable<any> {
    return this.http.get<any>(`${this._apiUrl}${this._specialEventsEndpoint}`);
  }

}
