import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _apiUrl = 'http://localhost:3000/api';
  private _signupEndpoint = '/register';
  private _signinEndpoint = '/login';

  constructor(private http: HttpClient, private router: Router) { }

  signUpUser(userData: any): Observable<any> {
    return this.http.post<any>(`${this._apiUrl}${this._signupEndpoint}`, userData);
  }

  signInUser(userData: any): Observable<any> {
    return this.http.post<any>(`${this._apiUrl}${this._signinEndpoint}`, userData);
  }

  signedInUser(): boolean {
    return !!localStorage.getItem('jwt');
  }

  getSignedInUserJwt() {
    return localStorage.getItem('jwt');
  }

  signOutUser() {
    localStorage.removeItem('jwt');
    this.router.navigate(['/events']);
  }
}
