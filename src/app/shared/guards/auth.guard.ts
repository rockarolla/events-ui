import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from "../services/auth.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(): boolean {
    const canActivate = this.authService.signedInUser();

    if (!canActivate) {
      this.router.navigate(['/signin']);
    }

    return canActivate;
  }
}
