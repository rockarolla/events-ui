import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SignupComponent} from './signup/signup.component';
import {SigninComponent} from "./signin/signin.component";
import {EventsComponent} from "./events/events.component";
import {SpecialEventsComponent} from "./special-events/special-events.component";
import {AuthGuard} from "./shared/guards/auth.guard";

const routes: Routes = [
  {path: 'signup', component: SignupComponent},
  {path: 'signin', component: SigninComponent},
  {path: 'events', component: EventsComponent},
  {path: 'special-events', component: SpecialEventsComponent, canActivate: [AuthGuard]},
  {path: '', redirectTo: '/events', pathMatch: 'full'},
  {path: '**', redirectTo: '/events', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
