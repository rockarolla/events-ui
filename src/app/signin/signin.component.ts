import { Component, OnInit } from '@angular/core';
import {AuthService} from "../shared/services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  userData: any = {
    email: '',
    password: ''
  };

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  signIn() {
    this.auth.signInUser(this.userData).subscribe(
      res => {
        console.log(res);
        localStorage.setItem('jwt', res.jwt);
        this.router.navigate(['/special-events']);
      },
      err => console.error(err)
    );
  }
}
