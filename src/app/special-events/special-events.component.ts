import { Component, OnInit } from '@angular/core';
import {EventsService} from "../shared/services/events.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-special-events',
  templateUrl: './special-events.component.html',
  styleUrls: ['./special-events.component.css']
})
export class SpecialEventsComponent implements OnInit {

  events = [];

  constructor(private eventsService: EventsService, private router: Router) { }

  ngOnInit() {
    this.eventsService.getSpecialEvents().subscribe(
      res => this.events = res,
      err => {
        console.error(err)
        if (err instanceof HttpErrorResponse && 401 === err.status) {
          this.router.navigate(['/signin']);
        }
      }
    );
  }

}
