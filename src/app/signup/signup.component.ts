import { Component, OnInit } from '@angular/core';
import {AuthService} from "../shared/services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  userData: any = {
    email: '',
    password: '',
    passwordConfirmation: ''
  };

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  signUp() {
    this.auth.signUpUser(this.userData).subscribe(
      res => {
        console.log(res);
        localStorage.setItem('jwt', res.jwt);
        this.router.navigate(['/special-events']);
      },
        err => console.error(err)
    );
  }
}
